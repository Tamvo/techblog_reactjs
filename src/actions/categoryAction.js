import { store } from "../store/store.js";
import {getEnv} from "../env";

let token = "Bearer" + " " + localStorage.getItem('token');

export const addCategory = async (name) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "createCategory", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: name
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "ADD_CATEGORY",
      dataAddCategory: await json.result
    });
  } catch (e) {
    return [e.response];
  }
};

export const updateCategory = async (category) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "updateCategory", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: category.id,
        name: category.name
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "UPDATE_CATEGORY",
      dataUpdateCategory: await json
    });
  } catch (e) {
    return [e.response];
  }
};

export const removeCategory = async (id) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "removeCategory", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: id
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "REMOVE_CATEGORY",
      dataRemoveCategory: await json
    });
  } catch (e) {
    return [e.response];
  }
};

export const getCategory = async () => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getCategories", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "GET_CATEGORY",
      dataGetCategory: await json.result
    });
  } catch (e) {
    return [e.response];
  }
};



