import { store } from "../store/store.js";
import {getEnv} from "../env";

let token = "Bearer" + " " + localStorage.getItem('token');

export const addComment = async (comment) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') +"createComment", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        content: comment.content,
        post_ID: comment.post_ID
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "ADD_COMMENT",
      dataAddComment: await json.result
    });
  } catch (e) {
    return [e.response];
  }
};

export const updateComment = async (comment) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') +"updateComment", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: comment.id,
        content: comment.content
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "UPDATE_COMMENT",
      dataUpdateComment: await json
    });
  } catch (e) {
    return [e.response];
  }
};

export const removeComment = async (id) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') +"removeComment", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: id
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "REMOVE_COMMENT",
      dataRemoveComment: await json
    });
  } catch (e) {
    return [e.response];
  }
};

