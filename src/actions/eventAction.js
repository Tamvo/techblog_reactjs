import { store } from "../store/store.js";
import { getEnv } from "../env";

let token = "Bearer" + " " + localStorage.getItem('token');
export const addEvent = async (data) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "createEvent", {
      method: "POST",
      headers: {
        'Authorization': token,
        "type": "formData"
      },
      body: data
    });
    const json = await response.json();
    store.dispatch({
      type: "ADD_EVENT",
      dataAddEvent: json.result
    });
  } catch (e) {
    return [e.response];
  }
};

export const updateEvent = async (event, id) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "updateEvent", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: id,
        name: event.name,
        description: event.description,
        start_date: event.start_date,
        end_date: event.end_date,
        image: event.image
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "UPDATE_EVENT",
      dataUpdateEvent: json
    });
  } catch (e) {
    return [e.response];
  }
};

export const removeEvent = async (id) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "removeEvent", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: id
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "REMOVE_EVENT",
      eventId: id,
      dataRemoveEvent: json
    });
  } catch (e) {
    return [e.response];
  }
};

export const getEvent = async () => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getEvents", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "GET_EVENT",
      dataEvent: json.result
    });
  } catch (e) {
    return [e.response];
  }
};

export const getEventDetail = async (id) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getEvent", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: id
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "GET_EVENT_DETAIL",
      dataGetEventDetail: json.result
    });
  } catch (e) {
    return [e.response];
  }
};


