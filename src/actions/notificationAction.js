import { store } from "../store/store.js";
import { getEnv } from "../env";

let token = "Bearer" + " " + localStorage.getItem('token');

const getCommentNotification = async () => {
    try {
        const response = await fetch(getEnv('REACT_APP_API_SERVER') + "notificationComment", {
            method: "POST",
            headers: {
                'Authorization': token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            })
        });
        const json = await response.json();
        return json.result;
    } catch (e) {
        return [e.response];
    }
};

const getLikeNotification = async () => {
    try {
        const response = await fetch(getEnv('REACT_APP_API_SERVER') + "notificationLikePost", {
            method: "POST",
            headers: {
                'Authorization': token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            })
        });
        const json = await response.json();
        return json.result;
    } catch (e) {
        return [e.response];
    }
};

const getQuestionNotification = async () => {
    try {
        const response = await fetch(getEnv('REACT_APP_API_SERVER') + "notificationQuestion", {
            method: "POST",
            headers: {
                'Authorization': token,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            })
        });
        const json = await response.json();
        return json.result;
    } catch (e) {
        return [e.response];
    }
};



export const fetchNotification = async () => {
    const dataCommentNo = await getCommentNotification();
    const dataLikeNo = await getLikeNotification();
    const dataQuestionNo = await getQuestionNotification();
    const count = dataCommentNo.length + dataLikeNo.length + dataQuestionNo.length;
    store.dispatch({
        type: "GET_NOTIFICATION",
        dataCommentNo,
        dataLikeNo,
        dataQuestionNo,
        count
    });
};