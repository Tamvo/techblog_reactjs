import { store } from "../store/store.js";
import {getEnv} from "../env";

const ACTIONS = {
  LOGOUT: "LOGOUT"
}


export const login = async (user) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "login", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username: user.username,
        password: user.password
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "LOGIN",
      dataLogin: json
    });
  } catch (e) {
    return [e.response];
  }
};

export const getUserInfo = async () => {
  try {
    let token = "Bearer" + " " + localStorage.getItem('token');
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getUserInfo", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "GET_USER_INFO",
      dataGetUser: json.result
    });
  } catch (e) {
    return [e.response];
  }
};

export const getUserInfoLogin = async () => {
  try {
    let token = "Bearer" + " " + localStorage.getItem('token');
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getUserInfoLogin", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "GET_USER_INFO_LOGIN",
      role: json.result.role,
      dataGetUserLogin: json.result
    });
  } catch (e) {
    return [e.response];
  }
};

export const getUsers = async () => {
  try {
    let token = "Bearer" + " " + localStorage.getItem('token');
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getUsers", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "GET_USERS",
      dataGetUsers: json.result
    });
  } catch (e) {
    return [e.response];
  }
};

export const changePassword = async (user) => {
  try {
    let token = "Bearer" + " " + localStorage.getItem('token');
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "changePassWord", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        oldPassword: user.oldPassword,
        newPassword: user.newPassword,
        confirmPassword: user.confirmPassword
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "CHANGE_PASSWORD",
      dataPassword: json.success
    });
  } catch (e) {
    return [e.response];
  }
};

export const register = async (user) => {
  try {
    const response = await fetch(getEnv('ID_APP_API_SERVER'), {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        action: "get_oauth_token",
        grantType: "password",
        grantData: [user.username, user.password]
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "REGISTER",
      dataRegister: json.result
    });
  } catch (e) {
    return [e.response];
  }
};

export const readUserIDInfo = async (token) => {
  try {
    const response = await fetch(getEnv('ID_APP_API_SERVER'), {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        action: "read_owned_info"
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "READ_USER_ID_INFO",
      dataUserID: json.result
    });
  } catch (e) {
    return [e.response];
  }
};

export const searchUser = async (username) => {
  let token = "Bearer " +localStorage.getItem('token');
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "search", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username: username
      })
    });
  
    const json = await response.json();
    store.dispatch({
      type: "SEARCH_USER",
      dataSearchUser: json.result
    });
    // console.log(json.result);
  } catch (e) {
    return [e.response];
  }
};

export const addUser = async (token, user) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "addUser", {
      method: "POST",
      headers: {
        'Authorization': "Bearer " + token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: user.id,
        username: user.username,
        firstname: user.firstname,
        lastname: user.lastname,
        birthday: user.birthday,
        gender: user.gender,
        email: user.email,
        phoneNumber: user.phoneNumber,
        profilePhoto: user.profilePhoto
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "ADD_USER",
      dataAddUser: json.success
    });
  } catch (e) {
    return [e.response];
  }
};

export const logout = () => {
  localStorage.removeItem('token');
  store.dispatch({
        type: ACTIONS.LOGOUT
       });
};

