import { store } from "../store/store.js";
import {getEnv} from "../env.js";

let token = "Bearer" + " " + localStorage.getItem('token');


export const getPermissions = async () => {
    try {
      const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getPermissions", {
        method: "POST",
        headers: {
          'Authorization': token,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
        })
      });
      const json = await response.json();
      store.dispatch({
        type: "GET_PERMISSION",
        dataGetPermission: json.result
      });
    } catch (e) {
      return [e.response];
    }
  };

  export const getRolePermissions = async (id) => {
    try {
      const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getRolePermissions", {
        method: "POST",
        headers: {
          'Authorization': token,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id: id
        })
      });
      const json = await response.json();
      store.dispatch({
        type: "GET_ROLE_PERMISSION",
        dataGetRolePermission: json.result
      });
    } catch (e) {
      return [e.response];
    }
  };