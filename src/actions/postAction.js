import { store } from "../store/store.js";
import {getEnv} from "../env";

let token = "Bearer" + " " + localStorage.getItem('token');
export const addPost = async (post) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "createPost", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: post.name,
        content: post.content,
        category_ID: post.category_ID
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "ADD_POST",
      dataAddPost: json.result
    });
  } catch (e) {
    return [e.response];
  }
};

export const updatePost = async (post, id) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "updatePost", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: id,
        name: post.name,
        content: post.content,
        category_ID: post.category_ID
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "UPDATE_POST",
      dataUpdatePost: json
    });
  } catch (e) {
    return [e.response];
  }
};

export const updateLikePost = async (id) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "likePost", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: id
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "UPDATE_LIKE_POST",
      postId: id,
      dataUpdateLikePost: json.success
    });
  } catch (e) {
    return [e.response];
  }
};

export const RemovePost = async (id) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "removePost", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
    
      body: JSON.stringify({
        id: id
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "REMOVE_POST",
      postId: id,
      dataRemovePost: json
    });
  } catch (e) {
    return [e.response];
  }
};

export const getPost = async () => {
      try {
        const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getCategoryPosts", {
          method: "POST",
          headers: {
            'Authorization': token,
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            category_ID: 0
          })
        });
        const json = await response.json();
        store.dispatch({
          type: "GET_POST",
          dataGetPost: json.result
        });
      } catch (e) {
        return [e.response];
      }
  };

  export const getCategoryPost = async (id) => {
      try {
          const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getCategoryPosts", {
              method: "POST",
              headers: {
                  'Authorization': token,
                  'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                  category_ID: id
              })
          });
          const json = await response.json();
        store.dispatch({
          type: "GET_CATEGORY_POST",
          dataGetCategoryPost: json.result
        });
      } catch (e) {
        return [e.response];
      }
  };

  export const getPostDetail = async (id) => {
      try {
        const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getPost", {
          method: "POST",
          headers: {
            'Authorization': token,
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            id: id
          })
        });
        const json = await response.json();
        store.dispatch({
          type: "GET_POST_DETAIL",
          dataGetPostDetail: json.result
        });
      } catch (e) {
        return [e.response];
      }
  };

  export const getActivePost = async () => {
    try {
      const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getActivePosts", {
        method: "POST",
        headers: {
          'Authorization': token,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
        })
      });
      const json = await response.json();
      store.dispatch({
        type: "GET_ACTIVE_POST",
        dataGetActivePost: json.result
      });
    } catch (e) {
      return [e.response];
    }
};

export const updateActivePost = async (id) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "updateActivePost", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: id,
        active: 1
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "UPDATE_ACTIVE_POST",
      postId: id,
      dataUpdateActivePost: json.result
    });
  } catch (e) {
    return [e.response];
  }
};

export const searchPost = async (name) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "searchPost", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: name
      })
    });
    const json = await response.json();
    // console.log("action" + json.result)
    store.dispatch({
      type: "SEARCH_POST",
      dataSearchPost: json.result
    });
  } catch (e) {
    return [e.response];
  }
};