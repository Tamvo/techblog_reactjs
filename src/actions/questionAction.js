import { store } from "../store/store.js";
import { getEnv } from "../env";

let token = "Bearer" + " " + localStorage.getItem('token');
export const addQuestion = async (question) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "createQuestion", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        content: question.content,
        parent_ID: question.parent_ID
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "ADD_QUESTION",
      dataAddQuestion: json
    });
  } catch (e) {
    return [e.response];
  }
};

export const updateQuestion = async (question) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "updateQuestion", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: question.id,
        content: question.content
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "UPDATE_QUESTION",
      question: question,
      dataUpdateQuestion: json.result
    });
  } catch (e) {
    return [e.response];
  }
};

export const removeQuestion = async (id) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "removeQuestion", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: id
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "REMOVE_QUESTION",
      questionId: id,
      dataRemoveQuestion: json
    });
  } catch (e) {
    return [e.response];
  }
};


export const getQuestion = async () => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getQuestions", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "GET_QUESTION",
      dataGetQuestion: json.result
    });
  } catch (e) {
    return [e.response];
  }
};

export const getQuestionDetail = async (id) => {
  try {
    const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getQuestion", {
      method: "POST",
      headers: {
        'Authorization': token,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: id
      })
    });
    const json = await response.json();
    store.dispatch({
      type: "GET_QUESTION_DETAIL",
      dataQuestionDetail: json.result
    });
  } catch (e) {
    return [e.response];
  }
};