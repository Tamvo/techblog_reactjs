import { store } from "../store/store.js";
import {getEnv} from "../env";

let token = "Bearer" + " " + localStorage.getItem('token');

export const addRole = async (data) => {
    try {
      const response = await fetch(getEnv('REACT_APP_API_SERVER') + "createRole", {
        method: "POST",
        headers: {
            'Authorization': token,
            'Content-Type': 'application/json'
        },
        body: data
      });
      const json = await response.json();
      store.dispatch({
        type: "ADD_ROLE",
        dataAddRole: json.result
      });
    } catch (e) {
      return [e.response];
    }
  };
export const getRoles = async () => {
    try {
      const response = await fetch(getEnv('REACT_APP_API_SERVER') + "getRoles", {
        method: "POST",
        headers: {
          'Authorization': token,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
        })
      });
      const json = await response.json();
      store.dispatch({
        type: "GET_ROLE",
        dataGetRole: json.result
      });
    } catch (e) {
      return [e.response];
    }
  };