import React from "react";
import "../../styles/styleHeader.css";
import { getUserInfo, logout } from "../../actions/oauthAction";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { fetchNotification } from "../../actions/notificationAction";

class Header extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleLogout = this.handleLogout.bind(this);
        this.handleList = this.handleList.bind(this);
        this.handleUserInfo = this.handleUserInfo.bind(this);
        this.state = {
            comment: true
        };
    }

    handleLogout(event) {
        event.preventDefault();
        logout();
        this.props.history.push("/login");
    }

    handleList() {
        this.setState({
            comment: !this.state.comment
        })
    }

    handleUserInfo(event) {
        event.preventDefault();
        this.props.history.push("/user-info");
    }

    componentDidMount() {
        if (localStorage.getItem('token')) {
            getUserInfo();
            fetchNotification();
        }
    }

    render() {
        return (
            <div>
                <div className="container-fluid nav-head-admin">
                    <div className="row">
                        <div className="col-12 col-md-9 headerBrand">
                            Techblog
                    </div>
                        <div className="col-12 col-md-1 headerAvatar">
                            <a href="javascript:void(0)" onClick={this.handleUserInfo}>
                                {this.props.dataUser ? <img className="avata" src={this.props.dataUser.profilePhoto} width="50" height="50" /> : <img src="https://cdn0.iconfinder.com/data/icons/avatars-6/500/Avatar_boy_man_people_account_client_male_person_user_work_sport_beard_team_glasses-512.png" width="50" height="50" />}
                                {this.props.dataUser ? this.props.dataUser.username : ""}
                            </a>
                        </div>
                        <div className="col-12 col-md-1 logoutButton">
                            <button className="btn-login" onClick={this.handleLogout}>Logout</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Header.defaultProps = {
    dataUser: null,
    dataCommentNo: [],
    dataLikeNo: [],
    dataQuestionNo: [],
    count: null
}

const mapStateToProps = (state) => {
    return {
        dataUser: state.handleUser.dataGetUser,
        dataCommentNo: state.handleNotification.dataCommentNo,
        dataLikeNo: state.handleNotification.dataLikeNo,
        dataQuestionNo: state.handleNotification.dataQuestionNo,
        count: state.handleNotification.count
    };
};
export default compose(withRouter, connect(mapStateToProps))(Header);