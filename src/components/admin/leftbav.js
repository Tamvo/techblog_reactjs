import React from "react";
import "../../styles/admin/styleIndex.css"
import { getActivePost } from "../../actions/postAction";
import { connect } from "react-redux";


class LefBav extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        getActivePost();
    }

    render() {
        return (
            <div className="left-bav">
                <ul>
                    <li>
                        <a onClick={this.handlePost} href="/admin/index">Action Post</a>
                        {this.props.dataActivePost ? 
                        <span className="float-right isAction">{(this.props.dataActivePost).length}</span>
                        : 0}
                    </li>
                    <li>
                        <a onClick={this.handleRole} href="/admin/role">Role and Permission</a>
                    </li>
                    <li>
                        <a onClick={this.handleRole} href="/admin/user-management">User management</a>
                    </li>
                </ul>
            </div>

        );
    }
}

LefBav.defaultProps = {
    dataActivePost: []
}
const mapStateToProps = (state) => {
    return {
        dataActivePost: state.handlePost.dataGetActivePost
    };
};

export default connect(mapStateToProps)(LefBav);

