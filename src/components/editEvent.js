import React from "react";
import "../styles/styleEditEvent.css";
class EditEvent extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="container">
                <form enctype="multipart/form-data">
                    <div className="row">
                        <div className="col-md-4">
                            <div className="form-group lable-input">
                                <label for="inputName">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" />
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group lable-input">
                                <label for="inputStartDate">Start date</label>
                                <input type="text" class="form-control" id="start_date" name="start_date" placeholder="Enter Start date" />
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group lable-input">
                                <label for="inputEndDate">End date</label>
                                <input type="text" class="form-control" id="end_date" name="end_date" placeholder="Enter end date" />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="form-group">
                                <input type="file" name="picture[]" multiple="multiple"/>
                            </div>
                        </div>

                    </div>
                    <div className="row">
                    <div className="col-md-12">
                            <div className="form-group lable-input">
                                <label for="inputDescription">Content</label>
                                <textarea rows="10" className="form-control" placeholder="Enter description"></textarea>
                            </div>
                        </div>
                    </div>
                    <button type="submit" className="btn btn-primary add-event">Edit Event</button>
                </form>
            </div>



            
          
        );
    }
}
export default EditEvent;