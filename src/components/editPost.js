import React from "react";
import "../styles/styleEditEvent.css";
class EditPost extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="container">
            <form onSubmit={this.handleAddPost}>
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group lable-input">
                            <label htmlFor="inputName">Name</label>
                            <input type="text" className="form-control" id="name" name="name" placeholder="Enter name" value="" required autoFocus/>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group lable-input">
                            <label htmlFor="inputName">category</label>
                            <select name="category" className="form-control">
                                <option select="selected" value="0"> category </option>
                                <option select="selected" value="0"> category </option>
                            </select>
                        </div>
                    </div>

                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="form-group lable-input">
                            <label htmlFor="inputName">Content</label>
                            <textarea rows="10" className="form-control"  id="content" name="content" placeholder="Enter content" value="" required autoFocus></textarea>
                        </div>
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">Add Post</button>
            </form>
        </div>



            
          
        );
    }
}
export default EditPost;