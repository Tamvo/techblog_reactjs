import React from "react";
import "../styles/styleHeader.css";
import { fetchNotification } from "../actions/notificationAction";
import { getUserInfo, logout } from "../actions/oauthAction";
import { getQuestionDetail } from "../actions/questionAction";
import { getPostDetail } from "../actions/postAction";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter, Redirect } from "react-router-dom";

class Header extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleLogout = this.handleLogout.bind(this);
        this.handleHome = this.handleHome.bind(this);
        this.handleEvent = this.handleEvent.bind(this);
        this.handleQuestion = this.handleQuestion.bind(this);
        this.handleList = this.handleList.bind(this);
        this.handleUserInfo = this.handleUserInfo.bind(this);
        this.handlePostDetail = this.handlePostDetail.bind(this);
        this.handleQuestionDetail = this.handleQuestionDetail.bind(this);
        this.state = {
            comment: false
        };
    }

    handleLogout(event) {
        event.preventDefault();
        logout();
        this.props.history.push("/login");
    }

    handleHome(event) {
        event.preventDefault();
        this.props.history.push("/");
    }
    handleEvent(event) {
        event.preventDefault();
        this.props.history.push("/event");
    }
    handleQuestion(event) {
        event.preventDefault();
        this.props.history.push("/question");
    }
    handleList() {
        this.setState({
            comment: !this.state.comment
        })
    }
    handleUserInfo(event) {
        event.preventDefault();
        this.props.history.push("/user-info");
    }
    handlePostDetail(id) {
        getPostDetail(id);
        if (this.props.dataGetPostDetail) {
            this.props.history.push("/post-detail");
        }
    }
    handleQuestionDetail(id) {
        getQuestionDetail(id);
        if (this.props.dataQuestionDetail) {
            this.props.history.push("/question-detail");
        }
    }
    componentDidMount() {
        if (localStorage.getItem('token')) {
            fetchNotification();
            getUserInfo();
        }
    }

    render() {
        return (
            <div className="header_home">
                {!localStorage.getItem('token') && <Redirect to="/login" />}
                <div className="container-fluid nav-head">
                    <div className="row">
                        <div className="col-12 col-md-9 headerBrand">
                            Techblog
                    </div>
                        <div className="col-12 col-md-1 headerAvatar">
                            <a href="javascript:void(0)" onClick={this.handleUserInfo} >
                                {this.props.dataUser ? <img className="avata" src={this.props.dataUser.profilePhoto} width="50" height="50" /> : <img src="https://cdn0.iconfinder.com/data/icons/avatars-6/500/Avatar_boy_man_people_account_client_male_person_user_work_sport_beard_team_glasses-512.png" width="50" height="50" />}
                                {this.props.dataUser ? this.props.dataUser.username : ""}
                            </a>
                        </div>
                        <div className="col-12 col-md-1 logoutButton">
                            <button className="btn-thongbao" onClick={this.handleList}>
                                <img className="img_thongbao" src="http://kclive.vn/themes/public/v2/img/ring.svg" width="35" height="35" />
                                {this.props.count ?
                                    <span className="thongbao">{this.props.count}</span>
                                    : ""}
                            </button>
                        </div>

                        <div className="list_thongbao">
                            <ul>
                                {(this.state.comment && this.props.dataQuestionNo) ? this.props.dataQuestionNo.map((question, index) =>
                                    <li key={index}>
                                        {!question.profilePhoto ?
                                            <img src="https://cdn0.iconfinder.com/data/icons/avatars-6/500/Avatar_boy_man_people_account_client_male_person_user_work_sport_beard_team_glasses-512.png" width="50" height="50" />
                                            :
                                            <img src={question.profilePhoto} width="50" height="50" />}
                                        <a href="javascript:void(0)" onClick={() => this.handleQuestionDetail(question.id)}><b>{question.username}</b> answered your question</a>
                                    </li>
                                ) : ""}
                                {(this.state.comment && this.props.dataLikeNo) ? this.props.dataLikeNo.map((post, index) =>
                                    <li key={index}>
                                        {!post.profilePhoto ?
                                            <img src="https://cdn0.iconfinder.com/data/icons/avatars-6/500/Avatar_boy_man_people_account_client_male_person_user_work_sport_beard_team_glasses-512.png" width="50" height="50" />
                                            :
                                            <img src={post.profilePhoto} width="50" height="50" />}
                                        <a href="javascript:void(0)" onClick={() => (this.handlePostDetail(post.id))}><b>{post.username}</b> liked your post</a>
                                    </li>
                                ) : ""}
                                {(this.state.comment && this.props.dataCommentNo) ? this.props.dataCommentNo.map((comment, index) =>
                                    <li key={index}>
                                        {!comment.profilePhoto ?
                                            <img src="https://cdn0.iconfinder.com/data/icons/avatars-6/500/Avatar_boy_man_people_account_client_male_person_user_work_sport_beard_team_glasses-512.png" width="50" height="50" />
                                            :
                                            <img src={comment.profilePhoto} width="50" height="50" />}
                                        <a href="javascript:void(0)" onClick={() => (this.handlePostDetail(comment.post_ID))}><b>{comment.username}</b> liked your post</a>
                                    </li>
                                ) : ""}
                            </ul>
                        </div>


                        <div className="col-12 col-md-1 logoutButton">
                            <button className="btn-login" onClick={this.handleLogout}>Logout</button>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row blankrow">
                        <div className="col-md-8">
                            <ul>
                                <li>
                                    <button className="button-header" onClick={this.handleHome}>Home</button>
                                </li>
                                <li>
                                    <button className="button-header" onClick={this.handleEvent}>Event</button>
                                </li>
                                <li>
                                    <button className="button-header" onClick={this.handleQuestion}>Question</button>
                                </li>
                            </ul>
                        </div>
                        
                    </div>

                    <div className="row">

                    </div>
                </div>
            </div>
        );
    }
}

Header.defaultProps = {
    dataUser: null,
    count: null,
    dataCommentNo: [],
    dataLikeNo: [],
    dataQuestionNo: [],
    dataQuestionDetail: null,
    dataGetPostDetail: null
}

const mapStateToProps = (state) => {
    return {
        dataUser: state.handleUser.dataGetUser,
        count: state.handleNotification.count,
        dataCommentNo: state.handleNotification.dataCommentNo,
        dataLikeNo: state.handleNotification.dataLikeNo,
        dataQuestionNo: state.handleNotification.dataQuestionNo,
        dataQuestionDetail: state.handleQuestion.dataQuestionDetail,
        dataGetPostDetail: state.handlePost.dataGetPostDetail
    };
};
export default compose(withRouter, connect(mapStateToProps))(Header);