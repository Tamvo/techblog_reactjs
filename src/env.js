const ENV = {
	LCL: {
		REACT_APP_API_SERVER: 'http://localhost:8000/',
		ID_APP_API_SERVER: 'https://api.dev-identity.bap.jp/api/rpc',
		REACT_APP_API_SERVER_IMAGE:'http://localhost:8000/files/'
	}
};

const config = ENV[process.env.REACT_APP_STAGE || "LCL"];

const getEnv = (name, defaultValue) => {
	return process.env[name] || config[name] || defaultValue;
};

export {getEnv};