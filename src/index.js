import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { store } from "./store/store";
import LoginComponent from "./views/login";
import LoginAdminComponent from "./views/admin/loginAdmin";
import RegisterComponent from "./views/registerID";
import HomeComponent from "./views/home";
import EventComponent from "./views/event";
import AddQuestionComponent from "./views/addQuestion";
import AddPostComponent from "./views/addPost";
import AddEventComponent from "./views/addEvent";
import EventDetailComponent from "./views/eventDetail";
import PostDetailComponent from "./views/postDetail";
import QuestionDetailComponent from "./views/questionDetail";
import IndexComponent from "./views/admin/indexAdmin";
import APostDetailComponent from "./views/admin/postDetail";
import RoleComponent from "./views/admin/role";
import UserComponent from "./views/admin/user";
import UserInfoComponent from "./views/userInfo";
import updateUserComponent from "./views/updateUser";
import changePasswordComponent from "./views/changePassword";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./styles.css";
import QuestionComponent from "./views/question";




function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route exact={true} path={"/register"} component={RegisterComponent} />
            <Route exact={true} path={"/login"} component={LoginComponent} />
            <Route exact={true} path={"/loginAdmin"} component={LoginAdminComponent} />
            <Route exact={true} path={"/"} component={HomeComponent} />
            <Route exact={true} path={"/event"} component={EventComponent} />
            <Route exact={true} path={"/question"} component={QuestionComponent} />
            <Route exact={true} path={"/add-question"} component={AddQuestionComponent} />
            <Route exact={true} path={"/add-post"} component={AddPostComponent} />
            <Route exact={true} path={"/add-event"} component={AddEventComponent} />
            <Route exact={true} path={"/event-detail"} component={EventDetailComponent} />
            <Route exact={true} path={"/post-detail"} component={PostDetailComponent} />
            <Route exact={true} path={"/question-detail"} component={QuestionDetailComponent} />
            <Route exact={true} path={"/admin/index"} component={IndexComponent} />
            <Route exact={true} path={"/admin/post-detail"} component={APostDetailComponent} />
            <Route exact={true} path={"/admin/role"} component={RoleComponent} />
            <Route exact={true} path={"/admin/user-management"} component={UserComponent} />
            <Route exact={true} path={"/user-info"} component={UserInfoComponent} />
            <Route exact={true} path={"/edit-user"} component={updateUserComponent} />
            <Route exact={true} path={"/change-password"} component={changePasswordComponent} />
          </Switch>
        </BrowserRouter>
      </div>
    </Provider>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
