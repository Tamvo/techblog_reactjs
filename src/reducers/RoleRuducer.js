const initialState = {
    dataGetRole: [],
   
};

const handleRole = (state = initialState, action = {}) => {
    switch (action.type) {
        case "GET_ROLE": {
            return {
                ...state,
                dataGetRole: action.dataGetRole,
            };
        };
        case "ADD_ROLE": {
            return {
                ...state,
                dataAddRole: action.dataAddRole,
            };
        };
        default:
            return state;
    }
};

export default handleRole;
