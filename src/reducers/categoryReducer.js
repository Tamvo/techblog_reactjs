const initialState = {
    dataGetCategory: [],
    dataAddCategory: null,
    dataUpdateCategory: null,
    dataRemoveCategory: null
};

const handleCategory = (state = initialState, action = {}) => {
    switch (action.type) {
        case "ADD_CATEGORY": {
            return {
                ...state,
                dataAddCategory: action.dataAddCategory,
            };
        };

        case "UPDATE_CATEGORY": {
            return {
                ...state,
                dataUpdateCategory: action.dataUpdateCategory,
            };
        };
        case "REMOVE_CATEGORY": {
            return {
                ...state,
                dataRemoveCategory: action.dataRemoveCategory,
            };
        };
        case "GET_CATEGORY": {
            return {
                ...state,
                dataGetCategory: action.dataGetCategory,
            };
        };
        default:
            return state;
    }
};

export default handleCategory;
