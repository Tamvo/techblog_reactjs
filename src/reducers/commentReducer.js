const initialState = {
    dataGetComment: [],
    dataAddComment: null,
    dataUpdateComment: null,
    dataRemoveComment: null
};

const handleComment = (state = initialState, action = {}) => {
    switch (action.type) {
        case "ADD_COMMENT": {
            return {
                ...state,
                dataAddComment: action.dataAddComment
            };
        };

        case "UPDATE_COMMENT": {
            return {
                ...state,
                dataUpdateComment: action.dataUpdateComment
            };
        };

        case "REMOVE_COMMENT": {
            return {
                ...state,
                dataRemoveComment: action.dataRemoveComment
            };
        };

        case "GET_COMMENT": {
            return {
                ...state,
                dataGetComment: action.dataGetComment
            };
        };

        default:
            return state;
    }
};

export default handleComment;