const initialState = {
    dataAddEvent: null,
    dataEvent: [],
    dataGetEventDetail: null,
    dataUpdateEvent: null,
    dataRemoveEvent: null
};

const handleEvent = (state = initialState, action = {}) => {
    switch (action.type) {
        case "ADD_EVENT": {
            return {
                ...state,
                dataAddEvent: action.dataAddEvent
            };
        };

        case "UPDATE_EVENT": {
            const data = [...state.dataEvent];
            return {
                ...state,
                dataEvent: data,
                dataUpdateEvent: action.dataUpdateEvent
            };
        };

        case "REMOVE_EVENT": {
            const data = [...state.dataEvent].filter(event => event.id !== action.eventId);
            return {
                ...state,
                dataEvent: data,
                dataRemoveEvent: action.dataRemoveEvent
            };
        };

        case "GET_EVENT": {
            return {
                ...state, 
            dataEvent: action.dataEvent
            };
        };

        case "GET_EVENT_DETAIL": {
            return {
                ...state,
                dataEventDetail: action.dataGetEventDetail
            };
        };

        default:
            return state;
    }
};

export default handleEvent;
