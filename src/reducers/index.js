import { combineReducers } from "redux";
import handleCategory from "./categoryReducer.js";
import handleEvent from "./eventReducer";
import handleComment from "./commentReducer.js";
import handleUser from "./oauthReducer";
import handlePost from "./postReducer";
import handleQuestion from "./questionReducer";
import handleRole from "./RoleRuducer";
import handlePermission from "./permissionReducer";
import handleNotification from "./notificationReducer";

const rootReducers = combineReducers({
    handleQuestion,
    handleCategory,
    handleEvent,
    handleComment,
    handleUser,
    handlePost,
    handleRole,
    handlePermission,
    handleNotification
});

export default rootReducers;
