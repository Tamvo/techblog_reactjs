const initialState = {
    dataCommentNo: [],
    dataLikeNo: [],
    dataQuestionNo: [],
    count: []
};

const handleNotification = (state = initialState, action = {}) => {
    switch (action.type) {
        case "GET_NOTIFICATION": {
            return {
                ...state,
                dataCommentNo: action.dataCommentNo,
                dataLikeNo: action.dataLikeNo,
                dataQuestionNo: action.dataQuestionNo,
                count: action.count
            };
        };

        default:
            return state;
    }
};

export default handleNotification;