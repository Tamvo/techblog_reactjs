const initialState = {
    dataLogin: null,
    dataGetUser: null,
    dataRegister: null,
    dataUserID: null,
    dataAddUser: null,
    userRole: null,
    dataGetUsers: null,
    role: null,
    dataSearchUser: null

};

const handleUser = (state = initialState, action = {}) => {
    switch (action.type) {
        case "LOGIN": {   
            localStorage.setItem('token', action.dataLogin.result.token);
            return {
                ...state,
                dataLogin: action.dataLogin.result.token
            };
        };

        case "GET_USER_INFO": {
            return {
                ...state,
                dataGetUser: action.dataGetUser
            };
        };
        case "GET_USERS": {
            return {
                ...state,
                dataGetUsers: action.dataGetUsers
            };
        };

        case "REGISTER": {
            return {
                ...state,
                dataRegister: action.dataRegister
            };
        };

        case "READ_USER_ID_INFO": {
            return {
                ...state,
                dataUserID: action.dataUserID
            };
        };

        case "SEARCH_USER": {
            return {
                ...state,
                dataSearchUser: action.dataSearchUser
            };
        };

        case "ADD_USER": {
            return {
                ...state,
                dataAddUser: action.dataAddUser
            };
        };

        case "LOGOUT": {
            return {
                ...state,
                dataLogin: null
            }
        };
        case "GET_USER_INFO_LOGIN":{
            console.log(action.role)
            return {
                ...state,
                role: action.role,
                dataGetUserLogin: action.dataGetUserLogin
            }
        };
        case "CHANGE_PASSWORD":{
            return {
                ...state,
                dataPassword: action.dataPassword
            }
        }


        default:
            return state;
    }
};

export default handleUser;
