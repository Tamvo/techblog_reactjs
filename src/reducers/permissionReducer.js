const initialState = {
    dataGetPermission: [],
    dataGetRolePermission: []
};

const handlePermission = (state = initialState, action = {}) => {
    switch (action.type) {
        case "GET_PERMISSION": {
            return {
                ...state,
                dataGetPermission: action.dataGetPermission,
            };
        };

        case "GET_ROLE_PERMISSION": {
            return {
                ...state,
                dataGetRolePermission: action.dataGetRolePermission,
            };
        };

        default:
            return state;
    }
};

export default handlePermission;
