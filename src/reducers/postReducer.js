const initialState = {
    dataAddPost: null,
    dataGetPost: [],
    dataGetPostDetail: null,
    dataCategoryPost: [],
    dataUpdatePost: null,
    dataRemovePost: null,
    dataUpdateLikePost: null,
    dataGetActivePost: [],
    dataUpdateActivePost: null
};

const handlePost = (state = initialState, action = {}) => {
    switch (action.type) {
        case "ADD_POST": {
            return {
                ...state,
                dataAddPost: action.dataAddPost
            };
        };

        case "UPDATE_POST": {
            return {
                ...state,
                dataUpdatePost: action.dataUpdatePost
            };
        };

        case "UPDATE_LIKE_POST": {
            if (action.dataUpdateLikePost) {
                const data = [...state.dataGetPost].map(post => {
                    if (post.id === action.postId) {
                        return {
                            ...post,
                            like: post.like + 1
                        }
                    }
                    return post;
                });
                const dataCategory = [...state.dataCategoryPost].map(post => {
                    if (post.id === action.postId) {
                        return {
                            ...post,
                            like: post.like + 1
                        }
                    }
                    return post;
                });
                return {
                    ...state,
                    dataGetPost: data,
                    dataCategoryPost: dataCategory,
                    dataUpdateLikePost: action.dataUpdateLikePost
                };
            } else {
                const data = [...state.dataGetPost].map(post => {
                    if (post.id === action.postId) {
                        return {
                            ...post,
                            like: post.like - 1
                        }
                    }
                    return post;
                });
                const dataCategory = [...state.dataCategoryPost].map(post => {
                    if (post.id === action.postId) {
                        return {
                            ...post,
                            like: post.like - 1
                        }
                    }
                    return post;
                });
                return {
                    ...state,
                    dataGetPost: data,
                    dataCategoryPost: dataCategory,
                    dataUpdateLikePost: action.dataUpdateLikePost
                };
            }
        };

        case "REMOVE_POST": {
            const data = [...state.dataGetPost].filter(post => post.id !== action.postId);
            const dataCategory = [...state.dataCategoryPost].filter(post => post.id !== action.postId);
            return {
                ...state,
                dataGetPost: data,
                dataCategoryPost: dataCategory,
                dataRemovePost: action.dataRemovePost
            };
        };

        case "GET_POST": {
            return {
                ...state,
                dataGetPost: action.dataGetPost
            };
        };

        case "GET_POST_DETAIL": {
            return {
                ...state,
                dataGetPostDetail: action.dataGetPostDetail
            };
        };

        case "GET_CATEGORY_POST": {
            return {
                ...state,
                dataCategoryPost: action.dataGetCategoryPost
            };
        };

        case "GET_ACTIVE_POST": {
            return {
                ...state,
                dataGetActivePost: action.dataGetActivePost
            };
        };


        case "SEARCH_POST": {
            return {
                ...state,
                dataSearchPost: action.dataSearchPost
            };
        };

        case "UPDATE_ACTIVE_POST": {
            const data = [...state.dataGetActivePost].filter(post => post.id !== action.postId);
            return {
                ...state,
                dataGetActivePost: data,
                dataUpdateActivePost: action.dataUpdateActivePost
            };
        };

        default:
            return state;
    }
};

export default handlePost;
