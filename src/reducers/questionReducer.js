const initialState = {
    dataAddQuestion: null,
    dataGetQuestion: [],
    dataUpdateQuestion: null,
    dataRemoveQuestion: null,
    dataQuestionDetail: null
};

const handleQuestion = (state = initialState, action = {}) => {
    switch (action.type) {
        case "ADD_QUESTION": {
            return {
                ...state,
                dataAddQuestion: action.dataAddQuestion
            };
        };

        case "UPDATE_QUESTION": {
            const data = [...state.dataGetQuestion].map(question => {
                if (question.id === action.question.id) {
                    return {
                        ...question,
                        content: action.question.content
                    }
                }
                return question;
            });
            return {
                ...state,
                dataGetQuestion: data,
                dataUpdateQuestion: action.dataUpdateQuestion
            };
        };

        case "REMOVE_QUESTION": {
            const data = [...state.dataGetQuestion].filter(question => question.id !== action.questionId);
            return {
                ...state,
                dataGetQuestion: data,
                dataRemoveQuestion: action.dataRemoveQuestion
            };
        };

        case "GET_QUESTION": {
            return {
                ...state,
                dataGetQuestion: action.dataGetQuestion
            };
        };

        case "GET_QUESTION_DETAIL": {
            return {
                ...state,
                dataQuestionDetail: action.dataQuestionDetail
            };
        };

        default:
            return state;
    }
};

export default handleQuestion;
