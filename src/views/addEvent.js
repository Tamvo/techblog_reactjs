import React from "react";
import Header from "../components/header";
import "../styles/styleAddEvent.css";
import { connect } from "react-redux";
import { addEvent, updateEvent } from "../actions/eventAction";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import DatePicker from 'react-datepicker';
import moment from 'moment'; 
import 'react-datepicker/dist/react-datepicker.css';

class AddEventComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleStart_dateChange = this.handleStart_dateChange.bind(this);
        this.handleEnd_dateChange = this.handleEnd_dateChange.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleAddEvent = this.handleAddEvent.bind(this);
        this.state = {
            name: "",
            description: "",
            start_date: "",
            end_date: "",
            images: [],
            id: "",
            dataPostDetail: null
        };
    }

    handleNameChange(event) {
        event.preventDefault();
        this.setState({
            name: event.target.value
        });
    }

    handleDescriptionChange(event) {
        event.preventDefault();
        this.setState({
            description: event.target.value
        });
    }
    handleStart_dateChange(date) {
        this.setState({
            //start_date: moment(date).format('DD/MM/YYYY')
             start_date: date.toDate()
        });
      }
      handleEnd_dateChange(date) {
        this.setState({
            //end_date: moment(date).format('DD/MM/YYYY')
            end_date: date.toDate()
        });
      }
   
    handleImageChange(event) {
        event.preventDefault();
        this.setState({
            images: event.target.files
        });
    }

    handleAddEvent(e) {
        e.preventDefault();
        let event = new FormData();
        event.append("name", this.state.name);
        event.append("description", this.state.description);
        event.append("start_date", this.state.start_date);
        event.append("end_date", this.state.end_date);
        var ins = document.getElementById('images').files.length;
        for (var i = 0; i < ins; i++) {
            event.append("image[]", document.getElementById('images').files[i]);
        }
        if (this.state.id) {
            updateEvent(event, this.state.id);
        } else {
            addEvent(event);
        }
        return this.props.history.push("/event");
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.dataEventDetail !== nextProps.dataEventDetail) {
            this.setState({
                id: nextProps.dataEventDetail.id,
                name: nextProps.dataEventDetail.name,
                description: nextProps.dataEventDetail.description,
                start_date: nextProps.dataEventDetail.start_date,
                end_date: nextProps.dataEventDetail.end_date
            });
        }
    }

    render() {
        return (
            <div>
                <Header />
                <div className="container">
                    <form encType="multipart/form-data">
                        <div className="row">
                            <div className="col-md-4">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputName">Name</label>
                                    <input type="text" className="form-control" id="name" name="name" placeholder="Enter name" value={this.state.name} onChange={this.handleNameChange} required autoFocus />
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputStartDate">Start date</label>
                                    <DatePicker
                                    selected={this.state.start_date}
                                        onChange={this.handleStart_dateChange}
                                        id="start_date" name="start_date"
                                        value={this.state.start_date}
                                        className="form-control" 
                                        required autoFocus
                                    />
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputEndDate">End date</label>
                                    <DatePicker
                                         selected={this.state.end_date}
                                        onChange={this.handleEnd_dateChange}
                                        id="end_date" name="end_date"
                                        value={this.state.end_date}
                                        className="form-control" 
                                        required autoFocus
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-4">
                                <div className="form-group">
                                    <input type="file" name="images" id="images" multiple onChange={this.handleImageChange} required autoFocus/>
                                </div>
                            </div>

                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputDescription">Content</label>
                                    <textarea rows="10" className="form-control" placeholder="Enter description" name="description" value={this.state.description} onChange={this.handleDescriptionChange} required autoFocus></textarea>
                                </div>
                            </div>
                        </div>
                        <button type="button" className="btn btn-primary add-event" onClick={this.handleAddEvent} >Add Event</button>
                    </form>
                </div>

            </div>
        );
    }
}

AddEventComponent.defaultProps = {
    dataEventDetail: null,
    dataAddEvent: null
}
const mapStateToProps = (state) => {
    return {
        dataEventDetail: state.handleEvent.dataEventDetail,
        dataAddEvent: state.handleEvent.dataAddEvent
    };
};

export default compose(withRouter, connect(mapStateToProps))(AddEventComponent);

