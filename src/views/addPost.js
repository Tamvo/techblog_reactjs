import React from "react";
import Header from "../components/header";
import "../styles/styleAddPost.css";
import { getCategory } from "../actions/categoryAction";
import { connect } from "react-redux";
import { addPost, updatePost } from "../actions/postAction";
import { compose } from "redux";
import { withRouter } from "react-router-dom";


class AddPostComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleAddPost = this.handleAddPost.bind(this);
        this.handleContentChange = this.handleContentChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleCategoryChange = this.handleCategoryChange.bind(this);
        this.state = {
            category: "",
            name: "",
            content: "",
            dataPostDetail: null,
            category_name: "",
            id: ""
        };
    }

    componentDidMount() {
        if (localStorage.getItem('token')) {
            getCategory();
        }
        
    }

    handleAddPost(event) {
        event.preventDefault();
        let post = {
            name: this.state.name,
            content: this.state.content,
            category_ID: this.state.category
        };
        if (this.state.id) {
            updatePost(post, this.state.id);
        } else {
            addPost(post);
        }
        return this.props.history.push("/");
    }

    handleContentChange(event) {
        event.preventDefault();
        this.setState({
            content: event.target.value
        });
    }

    handleNameChange(event) {
        event.preventDefault();
        this.setState({
            name: event.target.value
        });
    }

    handleCategoryChange(event) {
        event.preventDefault();
        this.setState({
            category: event.target.value
        });
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.dataPostDetail !== nextProps.dataPostDetail) {
            this.setState({
                id: nextProps.dataPostDetail.id,
                category: nextProps.dataPostDetail.category_ID,
                category_name: nextProps.dataPostDetail.category_name,
                name: nextProps.dataPostDetail.name,
                content: nextProps.dataPostDetail.content
            });
        }
    }

    render() {
        return (
            <div>
                <Header />
                <div className="container">
                    <form>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputName">Name</label>
                                    <input type="text" className="form-control" id="name" name="name" placeholder="Enter name" value={this.state.name} onChange={this.handleNameChange} required autoFocus />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputName">category</label>
                                    <select name="category" onChange={this.handleCategoryChange} className="form-control">
                                        {this.state.category ? <option select="selected" value={this.state.category}> {this.state.category_name} </option>
                                            : <option select="selected" value="0"> category </option>}

                                        {this.props.dataCategory ? this.props.dataCategory.map(category =>
                                            <option key={category.id} value={category.id}> {category.name} </option>
                                        ) : ""}
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputName">Content</label>
                                    <textarea rows="10" className="form-control" id="content" name="content" placeholder="Enter content" value={this.state.content} onChange={this.handleContentChange} required autoFocus></textarea>
                                </div>
                            </div>
                        </div>
                        <button type="button" className="btn btn-primary" onClick={this.handleAddPost}>Add Post</button>
                    </form>
                </div>

            </div>
        );
    }
}

AddPostComponent.defaultProps = {
    dataCategory: [],
    dataPostDetail: null
}
const mapStateToProps = (state) => {
    return {
        dataCategory: state.handleCategory.dataGetCategory,
        dataPostDetail: state.handlePost.dataGetPostDetail
    };
};
export default compose(withRouter, connect(mapStateToProps))(AddPostComponent);

