import React from "react";
import Header from "../components/header";
import {addQuestion} from "../actions/questionAction";
import "../styles/styleAddQuestion.css";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

class AddQuestionComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleAddQuestion = this.handleAddQuestion.bind(this);
        this.state = {
            content: "",
            parent_ID: 0
        };
    }

    handleChange(event) {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({
          [name]: value
        });
      }

    handleAddQuestion(event) {
        event.preventDefault();
        let question = {
            content: this.state.content,
            parent_ID: this.state.parent_ID
        };
        addQuestion(question);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.dataAddQuestion !== nextProps.dataAddQuestion) {
            alert("Added question successfully!");
            return this.props.history.push("/question");
        }
    }

    render() {
        return (
            <div>
                <Header />
                <div className="container">
                <form onSubmit={this.handleAddQuestion}>
                    <div className="row">
                        <div className="col-md-12 content-question">
                            <div className="form-group">
                                <label>Content</label>
                                <textarea rows="10" className="form-control" placeholder="Enter question..." id="content" name="content" value={this.state.value} onChange={this.handleChange} required autoFocus ></textarea>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                    <button type="submit" className="btn btn-primary">Add Question</button>
                    </div>
                </form>
            </div>
            </div>

        );
    }
}


AddQuestionComponent.defaultProps = {
    dataAddQuestion: null
}

const mapStateToProps = (state) => {
    return {
        dataAddQuestion: state.handleQuestion.dataAddQuestion
    };
};
export default compose(withRouter, connect(mapStateToProps))(AddQuestionComponent);
