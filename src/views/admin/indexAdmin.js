import React from "react";
import Header from "../../components/admin/headerAdmin";
import LefBav from "../../components/admin/leftbav";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import {updateActivePost, getPostDetail} from "../../actions/postAction";
import "../../styles/admin/styleIndex.css"


class IndexComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleActionPost = this.handleActionPost.bind(this);
        this.handleApprovalPostDetail = this.handleApprovalPostDetail.bind(this);
        this.state = {
            isActive: false,
            id: ""
        };

    }

    handleActionPost(id) {
        this.setState({
            isActive: true,
            id: id
        });
        updateActivePost(id);
    }

    handleApprovalPostDetail(id){
        getPostDetail(id);
        this.props.history.push("/admin/post-detail");
    }
    render() {
        return (
            <div>
                <Header />
                <div className="container-fulid">
                    <LefBav />
                    <div className="right-bav">

                        <table className="table">
                            <thead className="thead-light">
                                <tr>
                                    <th>Category</th>
                                    <th>Post Name</th>
                                    <th>Active</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.dataActivePost ? this.props.dataActivePost.map((post, index) =>
                                    <tr key={index}>
                                        <td>{post.category_name}</td>
                                        <td><a href="javascript:void(0)" onClick = {()=> this.handleApprovalPostDetail(post.id)}>{post.name}</a></td>
                                        <td>
                                            {!(this.state.isActive && this.state.id === post.id)?
                                                <button className="btn btn-danger" onClick={() => this.handleActionPost(post.id)}>Action</button>
                                                : ""}
                                        </td>

                                    </tr>
                                ) : ""}
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        );
    }
}

IndexComponent.defaultProps = {
    dataActivePost: []
}
const mapStateToProps = (state) => {
    return {
        dataActivePost: state.handlePost.dataGetActivePost
    };
};

export default compose(withRouter, connect(mapStateToProps))(IndexComponent);

