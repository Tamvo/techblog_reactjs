import React from "react";
import Button from "../../components/button";
import "../../styles/styleLogin.css";
import { login, getUserInfoLogin} from "../../actions/oauthAction";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";


class LoginAdminComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.handleLoginAdmin = this.handleLoginAdmin.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      username: "",
      password: "",
      token: ""
    };
  }
  handleLoginAdmin(event) {
    event.preventDefault();
    let user = {
      username: this.state.username,
      password: this.state.password
    };
    login(user);
    if(this.props.token){
      getUserInfoLogin();
      if(this.props.userRole=== 'admin')
      {
        this.props.history.push("/admin/index");
      }
    }
    }

  handleChange(event) {
    event.preventDefault();
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  }

  
  render() {
    console.log("noiho" + this.props.token)
    return (
      <div className="main">
        <div className="form-cover">
          <form className="frLogin">
            <div className="inputUserName">
              <p>
                <label htmlFor="inputUserName">
                  Username:
                </label>
                <input
                  className="form-input"
                  type="text"
                  id="username"
                  name="username"
                  value={this.state.value}
                  onChange={this.handleChange}
                  required
                  autoFocus
                />
              </p>
            </div>
            <p>
              <label htmlFor="inputUserPassword">
                Password:
              </label>
              <input
                className="form-input"
                type="password"
                id="password"
                name="password"
                value={this.state.value}
                onChange={this.handleChange}
                required
                autoFocus
              />
            </p>
            <div>
              <Button className="btLogin" label="Login" type="button" onClick={this.handleLoginAdmin} />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

LoginAdminComponent.defaultProps = {
  token: null,
  userRole: null
}

const mapStateToProps = (state) => {
  return {
    token: state.handleUser.dataLogin,
    userRole: state.handleUser.role
  };
};

export default compose(withRouter, connect(mapStateToProps))(LoginAdminComponent);
