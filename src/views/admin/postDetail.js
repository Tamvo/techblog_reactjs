import React from "react";
import "../../styles/admin/styleIndex.css";
import Header from "../../components/admin/headerAdmin";
import LefBav from "../../components/admin/leftbav";
import { connect } from "react-redux";


class APostDetailComponent extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <Header />
                <div className="container-fuild">
                    <LefBav />
                    {this.props.dataPostDetail ?
                        <div className="right-bav">
                            <h3>{this.props.dataPostDetail.name}</h3>
                            <div className="content-post">
                                <p> {this.props.dataPostDetail.content}</p>
                            </div>

                        </div>
                        : ""}
                </div>

            </div>
        );
    }
}


APostDetailComponent.defaultProps = {
    dataGetPostDetail: []
}
const mapStateToProps = (state) => {
    return {
        dataPostDetail: state.handlePost.dataGetPostDetail
    };
};

export default connect(mapStateToProps)(APostDetailComponent);