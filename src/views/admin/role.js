import React from "react";
import "../../styles/admin/styleIndex.css";
import Header from "../../components/admin/headerAdmin";
import LefBav from "../../components/admin/leftbav";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { getRoles } from "../../actions/roleAction";
import { getPermissions, getRolePermissions } from "../../actions/permissionAction";


class RoleComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleAddClass = this.handleAddClass.bind(this);
        this.handleRolePermission = this.handleRolePermission.bind(this);
        this.state = {
            active: '',
            id: ""
        }
    }


    handleAddClass(id) {
        this.setState({
            active: true,
            id: id
        });
    }

    handleRolePermission(role_ID) {
        getRolePermissions(role_ID);
    }

    componentDidMount() {
        getRoles();
        getPermissions();
    }


    render() {
        return (
            <div>
                <Header />
                <div className="container-fulid">
                    <LefBav />
                    <div className="right-bav">
                        <h2>Roles and Permission</h2>
                        <div className="float-left Item-role">
                            <ul>
                                {this.props.dataRole ? this.props.dataRole.map(role =>
                                    <li className={(this.state.active && role.id === this.state.id) ? 'active' : ''} onClick={() => this.handleAddClass(role.id)} key={role.id}>
                                        <button className="btn-role" onClick={() => this.handleRolePermission(role.id)}>{role.name}</button>
                                    </li>
                                ) : ""}
                            </ul>
                        </div>
                        <div className="float-right Item-permission">
                            <h3>Permission</h3>
                            <ul>
                                <form>
                                    {this.props.dataRolePermission ? this.props.dataRolePermission.map((permission, index) =>
                                        <li key={index}>
                                            <input type="checkbox" />
                                            <span>{permission.name}</span>
                                        </li>
                                    ) : ""}
                                    <input type="submit" className="btn btn-success" onClick={this.handleUpdateRole} />
                                </form>


                            </ul>

                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
RoleComponent.defaultProps = {
    dataRole: [],
    dataPermission: [],
    dataRolePermission: []
}
const mapStateToProps = (state) => {
    return {
        dataRole: state.handleRole.dataGetRole,
        dataPermission: state.handlePermission.dataGetPermission,
        dataRolePermission: state.handlePermission.dataGetRolePermission
    };
};

export default compose(withRouter, connect(mapStateToProps))(RoleComponent);
