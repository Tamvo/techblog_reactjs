import React from "react";
import "../../styles/admin/styleIndex.css";
import Header from "../../components/admin/headerAdmin";
import LefBav from "../../components/admin/leftbav";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { getUsers, searchUser } from "../../actions/oauthAction";
import Button from "../../components/button";
import { getRoles } from "../../actions/roleAction";
class UserComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleAddClass = this.handleAddClass.bind(this);
        this.handleCheckRole = this.handleCheckRole.bind(this);
        this.handleSearchUser = this.handleSearchUser.bind(this);
        this.state = {
            active: '',
            id: "",
            username: ""
        }
    }


    handleAddClass(id) {
        this.setState({
            active: true,
            id: id,

        });
    }

    handleCheckRole(id) {
        alert(id)
    }


    handleSearchUser(event) {
        event.preventDefault();
        this.setState({
            username: event.target.value
        });
        searchUser(this.state.username);
    }

    componentDidMount() {
        getUsers();
        getRoles();
    }


    render() {
        return (
            <div>
                <Header />
                <div className="container-fulid">
                    <LefBav />
                    <div className="right-bav">
                        <h2>User management</h2>
                        {!this.props.dataSearchUser ?
                            <div className="float-left Item-role">
                                <input type="text" className="form-control" placeholder="Search" name="username" id="username" value={this.state.value} onChange={this.handleSearchUser} autoFocus />
                                <ul>
                                    {this.props.dataUser ? this.props.dataUser.map(user =>
                                        <li className={(this.state.active && user.id === this.state.id) ? 'active' : ''} onClick={() => (this.handleAddClass(user.id))} key={user.id}><button className="btn-role">{user.username}</button></li>
                                    ) : ""}
                                </ul>
                            </div>
                            : 
                            <div className="float-left Item-role">
                                <input type="text" className="form-control" placeholder="Search" name="username" id="username" value={this.state.value} onChange={this.handleSearchUser} autoFocus />
                                <ul>
                                    {this.props.dataSearchUser ? this.props.dataSearchUser.map(user =>
                                        <li className={(this.state.active && user.id === this.state.id) ? 'active' : ''} onClick={() => (this.handleAddClass(user.id))} key={user.id}><button className="btn-role">{user.username}</button></li>
                                    ) : ""}
                                </ul>
                            </div>
                        }
                        <div className="float-right Item-permission">
                            <h3>Role</h3>
                            <ul>
                                {this.props.dataRole ? this.props.dataRole.map((role, index) =>
                                    <li key = {index}>
                                        <input type="checkbox" onClick={() => (this.handleCheckRole(role.id))} />
                                        <span>{role.name}</span>
                                    </li>
                                ) : ""}

                            </ul>
                            <button className="btLogin btn btn-primary" label="Update" onClick={this.handleUpdateRole} >Update</button>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}
UserComponent.defaultProps = {
    dataRole: [],
    dataUser: [],
    dataSearchUser: []
}
const mapStateToProps = (state) => {
    return {
        dataRole: state.handleRole.dataGetRole,
        dataUser: state.handleUser.dataGetUsers,
        dataSearchUser: state.handleUser.dataSearchUser
    };
};

export default compose(withRouter, connect(mapStateToProps))(UserComponent);
