import React from "react";
import Header from "../components/header";
import { changePassword } from "../actions/oauthAction";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";


class changePasswordComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state ={
            id: "",
            oldPassword: "",
            newPassword: "",
            confirmPassword: ""
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeClick = this.handleChangeClick.bind(this);

    }
    handleChangeClick(event) {
        event.preventDefault();
        let user = {
            oldPassword: this.state.oldPassword,
            newPassword: this.state.newPassword,
            confirmPassword: this.state.confirmPassword,
        };
        changePassword(user);
        return this.props.history.push("/login");
      }
    
    
      handleChange(event) {
        event.preventDefault();
        const { name, value } = event.target;
        this.setState({
          [name]: value
        });
       
      }


    render() {
        return (
            <div>
                <Header />
                <div className="container">
                    <form>
                        <div className="row">
                            <div className="col-md-4">
                                <label>Old password</label>
                                <input type="text" name="oldPassword" id="odlPassword" value={this.state.value} className="form-control" onChange={this.handleChange} />
                            </div>
                            <div className="col-md-4">
                                <label>New password</label>
                                <input type="text" name="newPassword" id="newPassword" value={this.state.value} className="form-control" onChange={this.handleChange} />
                            </div>
                            <div className="col-md-4">
                                <label>Confirm password</label>
                                <input type="text" name="confirmPassword" id="confirmPassword" value={this.state.value} className="form-control" onChange={this.handleChange} />
                            </div>
                           
                        </div>
                        <br />
                        <button className="btn btn-success" onClick={this.handleChangeClick}>Update</button>
                    </form>
                </div>
            </div >
        );

    }

}

changePasswordComponent.defaultProps = {
    dataPassword: null

}
const mapStateToProps = (state) => {
    return {
        dataPassword: state.handleUser.dataPassword
    };
};

export default compose(withRouter, connect(mapStateToProps))(changePasswordComponent);

