import React from "react";
import Header from "../components/header";
import "../styles/styleEvent.css";
import {getEventDetail, removeEvent, getEvent} from "../actions/eventAction";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import {getEnv} from "../env";

class EventComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleDeleteEvent = this.handleDeleteEvent.bind(this);
        this.handleUpdateEvent = this.handleUpdateEvent.bind(this);
        this.handleEventDetail = this.handleEventDetail.bind(this);
    };

    componentDidMount() {
        if (localStorage.getItem('token')) {
        getEvent();
        }
    }

    handleUpdateEvent(id){
        getEventDetail(id);
        this.props.history.push("/add-event");
    }

    handleEventDetail(id) {
        getEventDetail(id);
        if(this.props.dataEventDetail){
            this.props.history.push("/event-detail");
        }
    }

    handleDeleteEvent(id) {
        removeEvent(id);
        if (this.props.dataRemoveEvent) {
            alert("remove successfully!");
        }
    }

    render() {
        return (
            <div>
                <Header />
                <div className="container">
                    <div className="row">
                        <div className="add">
                            <a href="/add-event">Add Event</a>
                        </div>
                    </div>
                    {this.props.dataEvent ? this.props.dataEvent.map((event,index) =>
                        <div className="row box_event" key = {index}>
                            <div className="title col-md-10">
                                <a href="javascript:void(0)" onClick = {()=> (this.handleEventDetail(event.id))}><h3>{event.name}</h3> </a>
                                <span>{event.start_date}---{event.end_date}</span>
                            </div>
                            <div className="col-md-2">
                                <span className="edit">
                                    <a href="javascript:void(0)" >
                                        <img onClick={() => (this.handleDeleteEvent(event.id))} src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLDHNuQtxPRnqVmxqmj7ekuRWoTrnxCYW2XiewOuDOc_tw21gy3A" className="img_edit" width="20" height="20" />
                                    </a>
                                </span>
                                <span className="edit">
                                    <a href="javascript:void(0)" >
                                        <img onClick={() => (this.handleUpdateEvent(event.id))} src=" https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuJACzhPUAp6y7P9qP9irDFkscwVHi6XmjdcwXns_nPh_ryjW4bQ" className="img_edit" width="20" height="20" />
                                    </a>
                                </span>
                            </div>
                            <div className="col-md-3 col-12">
                                <img className="img_event" src={getEnv('REACT_APP_API_SERVER_IMAGE')+event.image[0] }/>
                            </div>
                            <div className="col-md-9">
                                <p>{event.description.substring(0, 400) + "..."}</p>
                            </div>
                            <div className="continue_read">
                                <a href="javascript:void(0)" onClick={()=> (this.handleEventDetail(event.id))}>+ Continue to reading</a>
                            </div>

                        </div>
                    ) : " "}
                </div>

            </div>
        );
    }
}

EventComponent.defaultProps = {
    dataEvent: [],
    dataEventDetail: null,
    dataRemoveEvent: null
}

const mapStateToProps = (state) => {
    return {
        dataEvent: state.handleEvent.dataEvent,
        dataEventDetail: state.handleEvent.dataEventDetail,
        dataRemoveEvent: state.handleEvent.dataRemoveEvent
    };
};

export default compose(withRouter, connect(mapStateToProps))(EventComponent);
