import React from "react";
import Header from "../components/header";
import { connect } from "react-redux";
import "../styles/styleEventDetail.css";
import {getEnv} from "../env";

class EventDetailComponent extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Header />
                {this.props.dataEventDetail ?
                    <div className="container detail-event">
                        <div id="demo" className="carousel slide" data-ride="carousel">
                            <ul className="carousel-indicators">
                                <li data-target="#demo" data-slide-to="0" className="active"></li>
                                <li data-target="#demo" data-slide-to="1"></li>
                                <li data-target="#demo" data-slide-to="2"></li>
                            </ul>
                            <div className="carousel-inner slide-img">
                                {this.props.dataEventDetail.image ? this.props.dataEventDetail.image.map(image =>
                                    <div className="carousel-item " key={image}>
                                        <img src={'http://localhost:8000/files/'+ this.props.dataEventDetail.image[0]} />
                                    </div>
                                ) :""}
                            </div>
                            <a className="carousel-control-prev" href="#demo" data-slide="prev">
                                <span className="carousel-control-prev-icon"></span>
                            </a>
                            <a className="carousel-control-next" href="#demo" data-slide="next">
                                <span className="carousel-control-next-icon"></span>
                            </a>
                        </div>
                        <h3>{this.props.dataEventDetail.name}</h3>
                        <span className="detail_event">
                            <span>
                                {this.props.dataEventDetail.profilePhoto ? <img src={this.props.dataEventDetail.profilePhoto} width="50" height="50" className="avata" /> :
                                    <img src="https://cdn0.iconfinder.com/data/icons/avatars-6/500/Avatar_boy_man_people_account_client_male_person_user_work_sport_beard_team_glasses-512.png" className="avata" width="50" height="50" />
                                }
                                {this.props.dataEventDetail.username}
                            </span>
                            <span>
                                {this.props.dataEventDetail.start_date}---{this.props.dataEventDetail.end_date}
                            </span>
                            <button type="submit" className="register-event">Register</button>
                        </span>
                        <div className="content-event">

                            <p> {this.props.dataEventDetail.description}</p>
                        </div>
                    </div>
                    : ""}
            </div>
        );
    }
}

EventDetailComponent.defaultProps = {
    dataEventDetail: []
}

const mapStateToProps = (state) => {
    return {
        dataEventDetail: state.handleEvent.dataEventDetail
    };
};
export default connect(mapStateToProps)(EventDetailComponent);
