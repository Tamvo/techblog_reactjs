import React from "react";
import Header from "../components/header";
import "../styles/styleHome.css";
import { getCategory } from "../actions/categoryAction";
import { getPost, RemovePost, getCategoryPost, getPostDetail, updateLikePost, searchPost } from "../actions/postAction";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter, Redirect } from "react-router-dom";

class HomeComponent extends React.PureComponent {
    constructor() {
        super();
        this.handleCategoryChange = this.handleCategoryChange.bind(this);
        this.handlePostDetail = this.handlePostDetail.bind(this);
        this.handleUpdatePost = this.handleUpdatePost.bind(this);
        this.handleDeletePost = this.handleDeletePost.bind(this);
        this.handleLike = this.handleLike.bind(this);
        this.handleChangeSearch = this.handleChangeSearch.bind(this);
        this.state = {
            category: 0,
            like: 0,
            dataCategoryPost: [],
            dataPost: [],
            name: ""
        };
    }

    handleLike(id) {
        updateLikePost(id);
    }

    handleUpdatePost(id) {
        getPostDetail(id);
        return this.props.history.push("/add-post");
    }

    handleDeletePost(id) {
        RemovePost(id);
        if (this.props.dataRemovePost) {
            alert("remove successfully!");
        }
    }

    componentDidMount() {
        if (this.props.dataLogin || localStorage.getItem('token')) {
            getCategory();
            getPost();
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.dataCategoryPost !== nextProps.dataCategoryPost) {
            this.setState({ dataCategoryPost: nextProps.dataCategoryPost });
        }

        if (this.props.dataPost !== nextProps.dataPost) {
            this.setState({ dataPost: nextProps.dataPost });
        }
    }
    handleChangeSearch(event) {
        event.preventDefault();
        this.setState({
            name: event.target.value
        });
        searchPost(this.state.name);


    }
    handleCategoryChange(event) {
        event.preventDefault();
        this.setState({ category: event.target.value }, () => getCategoryPost(this.state.category));
    }

    handlePostDetail(id) {
        getPostDetail(id);
        if (this.props.dataGetPostDetail) {
            this.props.history.push("/post-detail");
        }
    }

    render() {
        return (
            <div>
                {!(this.props.dataLogin || localStorage.getItem('token')) && <Redirect to="/login" />}
                <Header {...this.props} />
                <div className="container">
                    <div className="row">
                        <div className="add">
                            <a href="/add-post">Add Post</a>
                        </div>
                    </div>
                    <div className="row">
                        <select name="category" onChange={this.handleCategoryChange} className="custom-select col-md-3">
                            <option select="selected" value="0"> category </option>
                            {this.props.dataCategory ? this.props.dataCategory.map(category =>
                                <option key={category.id} value={category.id}> {category.name} </option>
                            ) : ""}
                        </select>
                        <div className="col-md-4 form-group">
                            <input type="text" className="form-control" name="name" id="name" placeholder="search..." onChange={this.handleChangeSearch} />
                            {/* <a href="">
                                <img src="https://www.freeiconspng.com/uploads/flat-search-find-icon-13.png" width="40" height="38" />
                            </a> */}
                        </div>
                    </div>

                    <div className="wrapper">
                        <div className="row">
                            {
                                this.props.dataSearchPost ?
                            this.state.category !== 0 ?
                                this.state.dataCategoryPost ? this.state.dataCategoryPost.map((post, index) =>
                                    <div className="col-md-6 Item-box" key={index}>
                                        <div className="Item-post">
                                            <h3> <a href="javascript:void(0)" onClick={() => (this.handlePostDetail(post.id))} > {post.name} </a></h3>
                                            <span className="edit">
                                                <a href="javascript:void(0)" onClick={() => (this.handleDeletePost(post.id))} >
                                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLDHNuQtxPRnqVmxqmj7ekuRWoTrnxCYW2XiewOuDOc_tw21gy3A" className="img_edit" width="20" height="20" />
                                                </a>
                                            </span>
                                            <span className="edit" >
                                                <a href="javascript:void(0)" onClick={() => (this.handleUpdatePost(post.id))} >
                                                    <img onClick={this.handleClickEdit} src=" https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuJACzhPUAp6y7P9qP9irDFkscwVHi6XmjdcwXns_nPh_ryjW4bQ" className="img_edit" width="20" height="20" />
                                                </a>
                                            </span>
                                            <p>
                                                {post.content.substring(0, 90) + "..."}
                                            </p>
                                            <div className="Item-post-detail">
                                                <div className="Item-detail">
                                                    <a href="">
                                                        {post.profilePhoto ? <img src={post.profilePhoto} width="40" height="40" className="avata" /> :
                                                            <img className="avata" src="https://cdn0.iconfinder.com/data/icons/avatars-6/500/Avatar_boy_man_people_account_client_male_person_user_work_sport_beard_team_glasses-512.png" width="40" height="40" />
                                                        }
                                                        <span>{post.username}</span>
                                                    </a>
                                                    <a href="javascript:void(0)" onClick={() => (this.handleLike(post.id))}>
                                                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfIvdIYvi3I21Kl1H6EsNWTXWLksBGWMHT9d5Ob6RdgDPvJZB7Dw" width="25" height="25" />
                                                    </a>
                                                    <span> {post.like} </span>
                                                    <a href="">
                                                        <img src="https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/1196900/1160/772/m1/fpnw/wm0/eye-icon-01-.jpg?1461166267&s=59ad886e0352a4a9a833b89b5ba6b030" width="50" height="40" />
                                                        <span> {post.view} </span>
                                                    </a>
                                                    <span> {post.created_at} </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ) : ""
                                :
                                 this.props.dataSearchPost.map((post, index) =>
                                    <div className="col-md-6 Item-box" key={index}>
                                        <div className="Item-post">
                                            <h3> <a href="javascript:void(0)" onClick={() => (this.handlePostDetail(post.id))} > {post.name} </a></h3>
                                            <span className="edit">
                                                <a href="javascript:void(0)" onClick={() => (this.handleDeletePost(post.id))} >
                                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLDHNuQtxPRnqVmxqmj7ekuRWoTrnxCYW2XiewOuDOc_tw21gy3A" className="img_edit" width="20" height="20" />
                                                </a>
                                            </span>
                                            <span className="edit" >
                                                <a href="javascript:void(0)" onClick={() => (this.handleUpdatePost(post.id))} >
                                                    <img onClick={this.handleClickEdit} src=" https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuJACzhPUAp6y7P9qP9irDFkscwVHi6XmjdcwXns_nPh_ryjW4bQ" className="img_edit" width="20" height="20" />
                                                </a>
                                            </span>
                                            <p>
                                                {post.content.substring(0, 90) + "..."}
                                            </p>
                                            <div className="Item-post-detail">
                                                <div className="Item-detail">
                                                    <a href="">
                                                        {post.profilePhoto ? <img src={post.profilePhoto} width="40" height="40" className="avata" /> :
                                                            <img className="avata" src="https://cdn0.iconfinder.com/data/icons/avatars-6/500/Avatar_boy_man_people_account_client_male_person_user_work_sport_beard_team_glasses-512.png" width="40" height="40" />
                                                        }
                                                        <span>{post.username}</span>
                                                    </a>
                                                    <a href="javascript:void(0)" onClick={() => (this.handleLike(post.id))}>
                                                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfIvdIYvi3I21Kl1H6EsNWTXWLksBGWMHT9d5Ob6RdgDPvJZB7Dw" width="25" height="25" />
                                                    </a>
                                                    <span> {post.like} </span>
                                                    <a href="">
                                                        <img src="https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/1196900/1160/772/m1/fpnw/wm0/eye-icon-01-.jpg?1461166267&s=59ad886e0352a4a9a833b89b5ba6b030" width="50" height="40" />
                                                        <span> {post.view} </span>
                                                    </a>
                                                    <span> {post.created_at} </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ) : 
                            
                            this.state.category !== 0 ?
                                this.state.dataCategoryPost ? this.state.dataCategoryPost.map((post, index) =>
                                    <div className="col-md-6 Item-box" key={index}>
                                        <div className="Item-post">
                                            <h3> <a href="javascript:void(0)" onClick={() => (this.handlePostDetail(post.id))} > {post.name} </a></h3>
                                            <span className="edit">
                                                <a href="javascript:void(0)" onClick={() => (this.handleDeletePost(post.id))} >
                                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLDHNuQtxPRnqVmxqmj7ekuRWoTrnxCYW2XiewOuDOc_tw21gy3A" className="img_edit" width="20" height="20" />
                                                </a>
                                            </span>
                                            <span className="edit" >
                                                <a href="javascript:void(0)" onClick={() => (this.handleUpdatePost(post.id))} >
                                                    <img onClick={this.handleClickEdit} src=" https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuJACzhPUAp6y7P9qP9irDFkscwVHi6XmjdcwXns_nPh_ryjW4bQ" className="img_edit" width="20" height="20" />
                                                </a>
                                            </span>
                                            <p>
                                                {post.content.substring(0, 90) + "..."}
                                            </p>
                                            <div className="Item-post-detail">
                                                <div className="Item-detail">
                                                    <a href="">
                                                        {post.profilePhoto ? <img src={post.profilePhoto} width="40" height="40" className="avata" /> :
                                                            <img className="avata" src="https://cdn0.iconfinder.com/data/icons/avatars-6/500/Avatar_boy_man_people_account_client_male_person_user_work_sport_beard_team_glasses-512.png" width="40" height="40" />
                                                        }
                                                        <span>{post.username}</span>
                                                    </a>
                                                    <a href="javascript:void(0)" onClick={() => (this.handleLike(post.id))}>
                                                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfIvdIYvi3I21Kl1H6EsNWTXWLksBGWMHT9d5Ob6RdgDPvJZB7Dw" width="25" height="25" />
                                                    </a>
                                                    <span> {post.like} </span>
                                                    <a href="">
                                                        <img src="https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/1196900/1160/772/m1/fpnw/wm0/eye-icon-01-.jpg?1461166267&s=59ad886e0352a4a9a833b89b5ba6b030" width="50" height="40" />
                                                        <span> {post.view} </span>
                                                    </a>
                                                    <span> {post.created_at} </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ) : ""
                                :
                                this.state.dataPost ? this.state.dataPost.map((post, index) =>
                                    <div className="col-md-6 Item-box" key={index}>
                                        <div className="Item-post" >
                                            <h3>  <a href="javascript:void(0)" onClick={() => (this.handlePostDetail(post.id))}>{post.name}</a> </h3>
                                            <span className="edit">
                                                <a href="javascript:void(0)" onClick={() => (this.handleDeletePost(post.id))}>
                                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLDHNuQtxPRnqVmxqmj7ekuRWoTrnxCYW2XiewOuDOc_tw21gy3A" className="img_edit" width="20" height="20" />
                                                </a>
                                            </span>
                                            <span className="edit">
                                                <a href="javascript:void(0)" onClick={() => (this.handleUpdatePost(post.id))} >
                                                    <img onClick={this.handleClickEdit} src=" https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuJACzhPUAp6y7P9qP9irDFkscwVHi6XmjdcwXns_nPh_ryjW4bQ" className="img_edit" width="20" height="20" />
                                                </a>
                                            </span>
                                            <p>
                                                {post.content.substring(0, 90) + "..."}
                                            </p>
                                            <div className="Item-post-detail">
                                                <div className="Item-detail">
                                                    <a href="">
                                                        {post.profilePhoto ? <img src={post.profilePhoto} width="40" height="40" className="avata" /> :
                                                            <img className="avata" src="https://cdn0.iconfinder.com/data/icons/avatars-6/500/Avatar_boy_man_people_account_client_male_person_user_work_sport_beard_team_glasses-512.png" width="40" height="40" />
                                                        }
                                                        <span>{post.username}</span>
                                                    </a>
                                                    <a href="javascript:void(0)" onClick={() => (this.handleLike(post.id))}>
                                                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfIvdIYvi3I21Kl1H6EsNWTXWLksBGWMHT9d5Ob6RdgDPvJZB7Dw" width="25" height="25" />
                                                    </a>
                                                    <span> {post.like} </span>
                                                    <a href="">
                                                        <img src="https://cmkt-image-prd.global.ssl.fastly.net/0.1.0/ps/1196900/1160/772/m1/fpnw/wm0/eye-icon-01-.jpg?1461166267&s=59ad886e0352a4a9a833b89b5ba6b030" width="50" height="40" />
                                                        <span> {post.view} </span>
                                                    </a>
                                                    <span> {post.created_at} </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                                    : ""
                            }
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

HomeComponent.defaultProps = {
    dataCategory: [],
    dataPost: [],
    dataCategoryPost: [],
    dataGetPostDetail: null,
    dataRemovePost: null,
    dataSearchPost: null
}
const mapStateToProps = (state) => {
    return {
        dataCategory: state.handleCategory.dataGetCategory,
        dataPost: state.handlePost.dataGetPost,
        dataCategoryPost: state.handlePost.dataCategoryPost,
        dataGetPostDetail: state.handlePost.dataGetPostDetail,
        dataRemovePost: state.handlePost.dataRemovePost,
        dataLogin: state.handleUser.dataLogin,
        dataSearchPost: state.handlePost.dataSearchPost
    };
};

export default compose(withRouter, connect(mapStateToProps))(HomeComponent);
