import React from "react";
import Button from "../components/button";
import "../styles/styleLogin.css";
import { login } from "../actions/oauthAction";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";


class LoginComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      username: "",
      password: "",
      token: ""
    };
  }
  handleLogin(event) {
    event.preventDefault();
    let user = {
      username: this.state.username,
      password: this.state.password
    };
    login(user);
  }


  handleChange(event) {
    event.preventDefault();
    const { name, value } = event.target;
    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <div className="main">
        {this.props.token && <Redirect to="/" />}
        <div className="form-cover">
          <form className="frLogin">
            <div className="inputUserName">
              <p>
                <label htmlFor="inputUserName">
                  Username:
                </label>
                <input
                  className="form-input"
                  type="text"
                  id="username"
                  name="username"
                  value={this.state.value}
                  onChange={this.handleChange}
                  required
                  autoFocus
                />
              </p>
            </div>
            <p>
              <label htmlFor="inputUserPassword">
                Password:
              </label>
              <input
                className="form-input"
                type="password"
                id="password"
                name="password"
                value={this.state.value}
                onChange={this.handleChange}
                required
                autoFocus
              />
            </p>
            <div>
              <Button className="btLogin" label="Login" type="button" onClick={this.handleLogin} />
            </div>
            <div>
              <a href="/register"> Register with Identity </a>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.handleUser.dataLogin
  };
};

export default connect(mapStateToProps)(LoginComponent);
