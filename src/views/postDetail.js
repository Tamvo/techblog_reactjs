import React from "react";
import Header from "../components/header";
import { connect } from "react-redux";
import { addComment } from "../actions/commentAction";
import "../styles/stylePostDetail.css";


class PostDetailComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleComment = this.handleComment.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            comment: ""
        };
    }

    handleComment(id) {
        let comment = {
            post_ID: id,
            content: this.state.comment
        }
        addComment(comment);

    }

    handleChange(event) {
        event.preventDefault();
        this.setState({
            comment: event.target.value
        });
    }

    render() {
        return (
            <div>
                <Header />
                {this.props.dataPostDetail ?
                    <div className="container detail-post">
                        <h3>{this.props.dataPostDetail.name}</h3>
                        <div className="content-post">
                            <p> {this.props.dataPostDetail.content}</p>
                        </div>
                        <h3>Comment</h3>
                        <form>
                            <textarea rows="2" cols="105" id="comment" name="comment" value={this.state.value} onChange={this.handleChange}></textarea>
                            <input type="button" className="btn btn-success" value="Comment" onClick={() => (this.handleComment(this.props.dataPostDetail.id))} />
                        </form>
                    </div>
                    : ""}
            </div>
        );
    }
}

PostDetailComponent.defaultProps = {
    dataPostDetail: []
}

const mapStateToProps = (state) => {
    return {
        dataPostDetail: state.handlePost.dataGetPostDetail
    };
};
export default connect(mapStateToProps)(PostDetailComponent);
