import React from "react";
import Header from "../components/header";
import "../styles/styleAnswer.css"
import "../styles/styleHeader.css"
import { getQuestion, getQuestionDetail, removeQuestion, updateQuestion } from "../actions/questionAction";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";

class QuestionComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleUpdateQuestion = this.handleUpdateQuestion.bind(this);
        this.handleDeleteQuestion = this.handleDeleteQuestion.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleContentChange = this.handleContentChange.bind(this);
        this.handleQuestionDetail = this.handleQuestionDetail.bind(this);
        this.state = {
            id: "",
            conteneditable: false,
            clickable: "",
            content: "",
            dataQuestion: []
        };
    }

    componentDidMount() {
        if (localStorage.getItem('token')) {
            getQuestion();
        }

    }

    componentWillReceiveProps(nextProps) {
        if (this.props.dataQuestion !== nextProps.dataQuestion) {
            this.setState({ dataQuestion: nextProps.dataQuestion });
        }
    }

    handleUpdateQuestion(event) {
        event.preventDefault();
        let question = {
            id: this.state.id,
            content: this.state.content
        };
        updateQuestion(question);
        this.setState({ conteneditable: !this.state.conteneditable });
    }

    handleChange(id, content) {
        this.setState({
            conteneditable: true,
            content: content,
            id: id
        });
    }
    handleContentChange(event) {
        event.preventDefault();
        this.setState({
            content: event.target.value
        });
    }
    handleDeleteQuestion(id) {
        removeQuestion(id);
        if (this.props.dataRemoveQuestion) {
            alert("remove successfully!");
        }
    }

    handleQuestionDetail(id) {
        getQuestionDetail(id);
        if (this.props.dataQuestionDetail) {
            this.props.history.push("/question-detail");
        }
    }

    render() {
        const { dataQuestion } = this.state
        return (
            <div>
                <Header />
                <div className="container">
                    <div className="row">
                        <div className="add">
                            <a href="/add-question">Add question</a>
                        </div>
                    </div>
                </div>
                {dataQuestion ? dataQuestion.map(question =>
                    <div className="container" key={question.id}>
                        <div className="row box-answer">
                            <div className="col-md-3 answer" >
                                <a href="">Answer </a>
                                <span> {question.answer} </span>

                            </div>
                            <div className="col-md-9 answer">
                                {!(this.state.conteneditable && (question.id === this.state.id)) ?
                                    <a href="javascript:void(0)" onClick={() => this.handleQuestionDetail(question.id)}>{question.content}</a> :
                                    <form>
                                        <input type="text" name="content" className="form-control ip-update" value={this.state.content} onChange={this.handleContentChange} required autoFocus />
                                        <input type="button" className="btn btn-success btn-update" value="update" onClick={this.handleUpdateQuestion} />
                                    </form>
                                }
                                <span className="edit">
                                    <a href="javascript:void(0)" onClick={() => this.handleDeleteQuestion(question.id)}>
                                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLDHNuQtxPRnqVmxqmj7ekuRWoTrnxCYW2XiewOuDOc_tw21gy3A" className="img_edit" width="20" height="20" />
                                    </a>
                                </span>
                                <span className="edit">
                                    <a href="javascript:void(0)" onClick={() => (this.handleChange(question.id, question.content))} >
                                        <img src=" https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuJACzhPUAp6y7P9qP9irDFkscwVHi6XmjdcwXns_nPh_ryjW4bQ" className="img_edit" width="20" height="20" />
                                    </a>
                                </span>
                            </div>
                            <div className="col-md-6">
                            </div>
                            <div className="col-md-6 detail_question">
                                {question.profilePhoto ? <img src={question.profilePhoto} width="50" height="50" className="avata" /> :
                                    <img src="https://cdn0.iconfinder.com/data/icons/avatars-6/500/Avatar_boy_man_people_account_client_male_person_user_work_sport_beard_team_glasses-512.png" width="50" height="46" className="avata" />
                                }
                                <a href="">{question.username}</a>
                                <img src="https://previews.123rf.com/images/magurok/magurok1408/magurok140800248/31022649-gold-coin-flat-icon.jpg" width="50" height="46" />
                                <span>20</span>
                                <span>{question.created_at}</span>
                            </div>
                        </div>
                    </div>
                ) : ""}
            </div>
        );

    }

}

QuestionComponent.defaultProps = {
    dataQuestion: [],
    dataRemoveQuestion: null,
    dataQuestionDetail: null,
    dataUpdateQuestion: null
}
const mapStateToProps = (state) => {
    return {
        dataQuestion: state.handleQuestion.dataGetQuestion,
        dataRemoveQuestion: state.handleQuestion.dataRemoveQuestion,
        dataQuestionDetail: state.handleQuestion.dataQuestionDetail,
        dataUpdateQuestion: state.handleQuestion.dataUpdateQuestion
    };
};

export default compose(withRouter, connect(mapStateToProps))(QuestionComponent);
