import React from "react";
import Header from "../components/header";
import "../styles/styleQuestionDetail.css";
import { connect } from "react-redux";
import {addQuestion} from "../actions/questionAction";


class QuestionDetailComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleAnswerQuestion = this.handleAnswerQuestion.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            content: ""
        };
    }

    handleChange(event){
        event.preventDefault();
        this.setState({
            content: event.target.value
        })
    }

    handleAnswerQuestion(id){
        let question = {
            parent_ID: id,
            content: this.state.content
        }
        addQuestion(question);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.dataAddQuestion !== nextProps.dataAddQuestion) {
            alert("Answer question successfully!");
            return this.props.history.push("/question");
        }
    }

    render() {
        return (
            <div>
                <Header />
                {this.props.dataQuestionDetail ?
                    <div className="container detail-post">
                        <h2>Question</h2>
                        <h3>{this.props.dataQuestionDetail.content}</h3>
                        <div className="content-post">
                            <h2>Answer</h2>
                            {this.props.dataQuestionDetail.answer.map((answer, index) =>
                                <div key={index}>
                                    <h4 className="stt"></h4>

                                    <p> {answer.content}</p>
                                </div>
                            )}
                        </div>
                        <div className="row">
                            <form>
                                <div className="col-md-12">
                                    <textarea rows="8" cols="100" name = "content" id = "content" value = {this.state.value} onChange = {this.handleChange}></textarea>
                                    <input type="button" className="btn btn-success" value="Answer"  onClick = {()=>this.handleAnswerQuestion(this.props.dataQuestionDetail.id)}/>
                                </div>

                            </form>
                        </div>
                    </div>
                    : ""}
            </div>
        );
    }
}

QuestionDetailComponent.defaultProps = {
    dataQuestionDetail: [],
    dataAddQuestion: null
}

const mapStateToProps = (state) => {
    return {
        dataQuestionDetail: state.handleQuestion.dataQuestionDetail,
        dataAddQuestion: state.handleQuestion.dataAddQuestion
    };
};
export default connect(mapStateToProps)(QuestionDetailComponent);
