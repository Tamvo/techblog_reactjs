import React from "react";
import Button from "../components/button";
import "../styles/styleLogin.css";
import { connect } from "react-redux";
import { register, readUserIDInfo, addUser } from "../actions/oauthAction";


class RegisterComponent extends React.Component {
  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      username: "",
      password: ""
    };
  }

  handleRegister(event) {
    event.preventDefault();
    let user = {
      username: this.state.username,
      password: this.state.password
    }
    register(user);
    if (this.props.dataRegister) {
      readUserIDInfo(this.props.dataRegister.token);
      if (this.props.dataUserID) {
        let token = this.props.dataRegister.token;
        let addedUser = {
          id: this.props.dataUserID.id,
          username: this.props.dataUserID.username,
          firstname: this.props.dataUserID.firstname,
          lastname: this.props.dataUserID.lastname,
          birthday: this.props.dataUserID.birthday,
          gender: this.props.dataUserID.gender,
          email: this.props.dataUserID.email,
          phoneNumber: this.props.dataUserID.phoneNumber,
          profilePhoto: this.props.dataUserID.profilePhoto
        }
        addUser(token, addedUser);
        if (this.props.dataAddUser) {
          return this.props.history.push("/login");
        }
      }
    }
  }

  handleChange(e) {
    e.preventDefault();
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <div className="main">
        <form onSubmit={this.handleRegister} className="frLogin">
          <div className="inputUsername">
            <p>
              <label htmlFor="inputUserName" >
                Username:
              </label>
              <input
                className="form-input"
                type="text"
                id="username"
                name="username"
                value={this.state.username}
                onChange={this.handleChange}
                required
                autoFocus
              />
            </p>
          </div>
          <p>
            <label htmlFor="inputUserPassword" >
              Password:
            </label>
            <input
              className="form-input"
              type="password"
              id="password"
              name="password"
              value={this.state.password}
              onChange={this.handleChange}
              required
              autoFocus
            />
          </p>
          <Button type="submit" className="btLogin" label="Register" />
        </form>
      </div>
    );
  }
}

RegisterComponent.defaultProps = {
  dataRegister: null,
  dataUserID: null,
  dataAddUser: null
}

const mapStateToProps = (state) => {
  return {
    dataRegister: state.handleUser.dataRegister,
    dataUserID: state.handleUser.dataUserID,
    dataAddUser: state.handleUser.dataAddUser
  };
};

export default connect(mapStateToProps)(RegisterComponent);

