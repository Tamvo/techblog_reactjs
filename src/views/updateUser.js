import React from "react";
import Header from "../components/header";
import "../styles/styleAddPost.css";
import { connect } from "react-redux";
import { getUserInfoLogin } from "../actions/oauthAction";
import { compose } from "redux";
import { withRouter } from "react-router-dom";


class updateUserComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleLastnameChange = this.handleLastnameChange.bind(this);
        this.handleFirstnameChange = this.handleFirstnameChange.bind(this);
        this.handleBirthdayChange = this.handleBirthdayChange.bind(this);
        this.handleGenderChange = this.handleGenderChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePhonenumberChange = this.handlePhonenumberChange.bind(this);
        this.handleUpdateUser = this.handleUpdateUser.bind(this);
        this.state = {
            id: "",
            username: "",
            password: "",
            firstname: "",
            lastname: "",
            birthday: "",
            gender: "",
            email: "",
            phoneNumber: ""
        };
    }

    componentDidMount() {
        getUserInfoLogin();

    }

    handleUpdateUser(event) {
        event.preventDefault();
        let user = {
            username: this.state.username,
            password: this.state.password,
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            birthday: this.state.birthday,
            gender: this.state.gender,
            email: this.state.email,
            phoneNumber: this.state.phoneNumber
        };

        // updatePost(user, this.state.id);

        return this.props.history.push("/");
    }

    handleUsernameChange(event) {
        event.preventDefault();
        this.setState({
            username: event.target.value
        });

    }
    handlePasswordChange(event) {
        event.preventDefault();
        this.setState({
            password: event.target.value
        });

    }
    handleLastnameChange(event) {
        event.preventDefault();
        this.setState({
            lastname: event.target.value
        });

    }
    handleFirstnameChange(event) {
        event.preventDefault();
        this.setState({
            firstname: event.target.value
        });

    }
    handleGenderChange(event) {
        event.preventDefault();
        this.setState({
            gender: event.target.value
        });

    }
    handleEmailChange(event) {
        event.preventDefault();
        this.setState({
            email: event.target.value
        });

    }
    handleBirthdayChange(event) {
        event.preventDefault();
        this.setState({
            birthday: event.target.value
        });

    }
    handlePhonenumberChange(event) {
        event.preventDefault();
        this.setState({
            phoneNumber: event.target.value
        });

    }



    componentWillReceiveProps(nextProps) {
        if (this.props.dataUser !== nextProps.dataUser) {

            this.setState({
                id: nextProps.dataUser.id,
                username: nextProps.dataUser.username,
                lastname: nextProps.dataUser.lastname,
                firstname: nextProps.dataUser.firstname,
                email: nextProps.dataUser.email,
                phoneNumber: nextProps.dataUser.phoneNumber,
                gender: nextProps.dataUser.gender,
                password: nextProps.dataUser.password,
                birthday: nextProps.dataUser.birthday,
            });
        }
    }

    render() {
        console.log("mjthnesri" + this.props.dataUser)
        return (
            <div>
                <Header />
                <div className="container">
                    <form>
                        <div className="row">
                            <div className="col-md-3">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputName">Username</label>
                                    <input type="text" className="form-control" id="username" name="username" value={this.state.username} onChange={this.handleUsernameChange} required autoFocus />
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputName">LastName</label>
                                    <input type="text" className="form-control" id="lastname" name="lastname" value={this.state.lastname} onChange={this.handleLastnameChange} required autoFocus />
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputName">FirstName</label>
                                    <input type="text" className="form-control" id="firstname" name="firstname" value={this.state.firstname} onChange={this.handleFirstnameChange} required autoFocus />
                                </div>
                            </div>


                        </div>
                        <div className="row">
                            <div className="col-md-3">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputName">Birthday</label>
                                    <input type="text" className="form-control" id="birthday" name="birthday" value={this.state.birthday} onChange={this.handleBirthdayChange} required autoFocus />
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputName">Email</label>
                                    <input type="text" className="form-control" id="email" name="email" value={this.state.email} onChange={this.handleEmailChange} required autoFocus />
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputName">Phone Number</label>
                                    <input type="text" className="form-control" id="phoneNumber" name="phoneNumber" value={this.state.phoneNumber} onChange={this.handlePhonenumberChange} required autoFocus />
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group lable-input">
                                    <label htmlFor="inputName">Gender</label>
                                    <div className="row">
                                        <div className="col-md-4">
                                            <input type="checkbox" className="form-control" name="gender" required autoFocus />
                                            Male
                                        </div>
                                        <div className="col-md-4">
                                            <input type="checkbox" className="form-control" name="gender"  required autoFocus />
                                            Female
                                        </div>
                                        <div className="col-md-4">
                                            <input type="checkbox" className="form-control" name="gender" value={this.state.gender} />
                                            Other
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <button type="button" className="btn btn-primary" onClick={this.handleUpdateUser}>Update</button>
                    </form>
                </div>

            </div>
        );
    }
}

updateUserComponent.defaultProps = {
    dataUser: null,
}
const mapStateToProps = (state) => {
    return {
        dataUser: state.handleUser.dataGetUserLogin
    };
};
export default compose(withRouter, connect(mapStateToProps))(updateUserComponent);

