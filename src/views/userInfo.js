import React from "react";
import Header from "../components/header";
import { connect } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { getUserInfoLogin } from "../actions/oauthAction";
import "../styles/admin/styleIndex.css";

class UserInfoComponent extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleEditUser = this.handleEditUser.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.state = {
            id: "",
            username: "",
            password: "",
            firstname: "",
            lastname: "",
            birthday: "",   
            gender: "",
            email: "",
            phoneNumber: "",
            profilePhoto: ""

        };
    }
    handleEditUser(event) {
        event.preventDefault();
        this.props.history.push("/edit-user");
    }
    handleChangePassword(event){
        event.preventDefault();
        this.props.history.push("/change-password");
    }
    componentDidMount() {
        getUserInfoLogin();
    }

    render() {
        return (
            <div>
                <Header />
                <div className="container">
                <button className="btn btn-primary change-pw" onClick={this.handleChangePassword} >Change password</button>
                    <h3>User Information</h3>
                    {this.props.dataUserInfo ?
                        <div className="user_info">

                            <ul>
                                <li>
                                    <h4>ID</h4>
                                    <span>{this.props.dataUserInfo.id}</span>
                                </li>
                                <li>
                                    <h4>UserName</h4>
                                    <span>{this.props.dataUserInfo.username}</span>
                                </li>
                                <li>
                                    <h4>FirstName</h4>
                                    <span>{this.props.dataUserInfo.firstname}</span>
                                </li>
                                <li>
                                    <h4>LastName</h4>
                                    <span>{this.props.dataUserInfo.lastname}</span>
                                </li>
                                <li>
                                    <h4>Gender</h4>
                                    <span>{this.props.dataUserInfo.gender}</span>
                                </li>
                                <li>
                                    <h4>Email</h4>
                                    <span>{this.props.dataUserInfo.email}</span>
                                </li>
                                <li>
                                    <h4>PhoneNumber</h4>
                                    <span>{this.props.dataUserInfo.phoneNumber}</span>
                                </li>
                                <li>
                                    <h4>Birthday</h4>
                                    <span>{this.props.dataUserInfo.birthday}</span>
                                </li>

                            </ul>
                            <button className="btn btn-success" onClick={this.handleEditUser}>Edit User</button>

                        </div>
                        : ""}
                </div>

            </div>
        );

    }

}

UserInfoComponent.defaultProps = {
    dataUserInfo: null

}
const mapStateToProps = (state) => {
    return {
        dataUserInfo: state.handleUser.dataGetUserLogin
    };
};

export default compose(withRouter, connect(mapStateToProps))(UserInfoComponent);
